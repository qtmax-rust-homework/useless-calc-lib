use std::f32;
use crate::Circle;
use crate::Rect;

pub trait Perimeter {
	fn perimeter(&self) -> f32;
}

impl Perimeter for Circle {
	fn perimeter(&self) -> f32 {
		f32::consts::PI * self.radius * 2.
	}
}

impl Perimeter for Rect {
	fn perimeter(&self) -> f32 {
		if self.width == 0. || self.height == 0. {
			self.width + self.height
		} else {
			(self.width + self.height) * 2.
		}
	}
}
