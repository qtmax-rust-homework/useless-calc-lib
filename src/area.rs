use std::f32;
use crate::Circle;
use crate::Rect;

pub trait Area {
	fn area(&self) -> f32;
}

impl Area for Circle {
	fn area(&self) -> f32 {
		f32::consts::PI * self.radius * self.radius
	}
}

impl Area for Rect {
	fn area(&self) -> f32 {
		self.width * self.height
	}
}
