use std::f32;
use crate::Sphere;
use crate::Cuboid;

pub trait Volume {
	fn volume(&self) -> f32;
}

impl Volume for Sphere {
	fn volume(&self) -> f32 {
		f32::consts::PI * self.radius * self.radius * self.radius * 4. / 3.
	}
}

impl Volume for Cuboid {
	fn volume(&self) -> f32 {
		self.width * self.height * self.depth
	}
}
