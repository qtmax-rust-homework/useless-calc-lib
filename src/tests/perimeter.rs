use std::f32::consts::PI;
use crate::{Circle, Rect};
use crate::perimeter::Perimeter;
use float_cmp::approx_eq;

#[test]
fn test_circle_perimeter_nonzero() {
	let c = Circle { radius: 10. };

	assert!(approx_eq!(f32, c.perimeter(), 20. * PI, ulps=2));
}

#[test]
fn test_circle_perimeter_zero_radius() {
	let c = Circle { radius: 0. };

	assert_eq!(c.perimeter(), 0.);
}

#[test]
fn test_rect_perimeter_nonzero() {
	let r = Rect { width: 20., height: 30. };

	assert!(approx_eq!(f32, r.perimeter(), 100., ulps=2));
}

#[test]
fn test_rect_perimeter_zero_width() {
	let r = Rect { width: 0., height: 10. };

	assert_eq!(r.perimeter(), 10.);
}

#[test]
fn test_rect_perimeter_zero_height() {
	let r = Rect { width: 10., height: 0. };

	assert_eq!(r.perimeter(), 10.);
}

#[test]
fn test_rect_perimeter_zero() {
	let r = Rect { width: 0., height: 0. };

	assert_eq!(r.perimeter(), 0.);
}
