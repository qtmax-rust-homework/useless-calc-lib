use std::f32::consts::PI;
use crate::{Circle, Rect};
use crate::area::Area;
use float_cmp::approx_eq;

#[test]
fn test_circle_area_nonzero() {
	let c = Circle { radius: 10. };

	assert!(approx_eq!(f32, c.area(), 100. * PI, ulps=2));
}

#[test]
fn test_circle_area_zero_radius() {
	let c = Circle { radius: 0. };

	assert_eq!(c.area(), 0.);
}

#[test]
fn test_rect_area_nonzero() {
	let r = Rect { width: 20., height: 30. };

	assert!(approx_eq!(f32, r.area(), 600., ulps=2));
}

#[test]
fn test_rect_area_zero_width() {
	let r = Rect { width: 0., height: 10. };

	assert_eq!(r.area(), 0.);
}

#[test]
fn test_rect_area_zero_height() {
	let r = Rect { width: 10., height: 0. };

	assert_eq!(r.area(), 0.);
}

#[test]
fn test_rect_area_zero() {
	let r = Rect { width: 0., height: 0. };

	assert_eq!(r.area(), 0.);
}
