use std::f32::consts::PI;
use crate::{Sphere, Cuboid};
use crate::volume::Volume;
use float_cmp::approx_eq;

#[test]
fn test_sphere_volume_nonzero() {
	let s = Sphere { radius: 10. };

	assert!(approx_eq!(f32, s.volume(), 4000. * PI / 3., ulps=2));
}

#[test]
fn test_sphere_volume_zero_radius() {
	let s = Sphere { radius: 0. };

	assert_eq!(s.volume(), 0.);
}

#[test]
fn test_cuboid_volume_nonzero() {
	let c = Cuboid { width: 10., height: 20., depth: 30. };

	assert_eq!(c.volume(), 6000.);
}

#[test]
fn test_cuboid_volume_zero_width() {
	let c = Cuboid { width: 0., height: 20., depth: 30. };

	assert_eq!(c.volume(), 0.);
}

#[test]
fn test_cuboid_volume_zero_height() {
	let c = Cuboid { width: 10., height: 0., depth: 30. };

	assert_eq!(c.volume(), 0.);
}

#[test]
fn test_cuboid_volume_zero_depth() {
	let c = Cuboid { width: 10., height: 20., depth: 0. };

	assert_eq!(c.volume(), 0.);
}

#[test]
fn test_cuboid_volume_zero() {
	let c = Cuboid { width: 0., height: 0., depth: 0. };

	assert_eq!(c.volume(), 0.);
}
