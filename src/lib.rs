#[cfg(test)]
mod tests;

pub mod area;
pub mod perimeter;
pub mod volume;

pub struct Circle {
	pub radius: f32,
}

pub struct Rect {
	pub width: f32,
	pub height: f32,
}

pub struct Sphere {
	pub radius: f32,
}

pub struct Cuboid {
	pub width: f32,
	pub height: f32,
	pub depth: f32,
}
